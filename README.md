# REDVENTURES
> http://mechamoallan.com.br/redventures/#/

This project uses a number of open source techs

* [AngularJS] - HTML enhanced for web apps!
* [Bower] - Package manager
* [node.js] - Evented I/O for the backend
* [Gulp] - The streaming build system
* [Stylus] - Expressive, dynamic and robust CSS
* [animate.css] - Simple and best animation lib for CSS
* [Browser Sync] - Time-saving synchronised browser testing
* [Flexbox Grid] - A grid system based on the flex display property


## Getting Started
Open your favorite Terminal and run these commands.

### Installation
Install dependences globally if necessary:
```bash
$ npm install --global gulp-cli
$ npm install -g browser-sync
$ npm install -g bower
```

### Run app
Build files and start the server
```sh
$ gulp build && gulp server
```
### Cool tasks
This project automate some stuffs with Gulp

##### useref
Parse build blocks in HTML files to replace references to non-optimized scripts or stylesheets.
```sh
$ gulp useref
```
##### stylus
Compile and compress styl to CSS.
```sh
$ gulp stylus
```

##### imagemin
Minify PNG, JPEG, GIF and SVG images
```sh
$ gulp imagemin
```
##### server
Live CSS Reload & Browser Syncing.
```sh
$ gulp server
```
##### build
Run **useref**, **stylus** and **imagemin**
```sh
$ gulp build
```
##### watch
Automate **useref**, **stylus** and **imagemin**
```sh
$ gulp watch
```
[Bower]: <https://bower.io/>
[Stylus]: <http://stylus-lang.com>
[animate.css]: <https://daneden.github.io/animate.css/>
[node.js]: <http://nodejs.org>
[AngularJS]: <http://angularjs.org>
[Gulp]: <http://gulpjs.com>
[Browser Sync]: <https://www.browsersync.io>
[Flexbox Grid]: <http://flexboxgrid.com/>
