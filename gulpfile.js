const gulp       = require('gulp'),
    stylus       = require('gulp-stylus'),
    cssnano      = require('gulp-cssnano'),
    autoprefixer = require('gulp-autoprefixer'),
    useref       = require('gulp-useref'),
    plumber      = require('gulp-plumber'),
    imagemin     = require('gulp-imagemin'),
    browserSync  = require('browser-sync').create()

gulp.task('useref', function () {
    return gulp.src('src/**/*.html')
		.pipe(plumber())
        .pipe(useref())
		.pipe(plumber.stop())
        .pipe(gulp.dest('./app'))
})

gulp.task('stylus', function(){
    return gulp.src('src/stylus/main.styl')
        .pipe(plumber())
        .pipe(stylus({
            compress: true
        }))
        .pipe(plumber.stop())
        .pipe(gulp.dest('./app/css'))
})

gulp.task('ioniconsDependences', function(){
    return gulp.src('src/components/ionicons-min/fonts/*')
        .pipe(gulp.dest('./app/fonts'))
})

gulp.task('imagemin', function(){
    return gulp.src('src/img/*')
        .pipe(imagemin())
        .pipe(gulp.dest('./app/img'))
})

gulp.task('server', function () {
    browserSync.init({
        server: {
            baseDir: "./app"
        }
    })
})

gulp.task('watch', function(){
    gulp.watch('src/*', ['useref'])
    gulp.watch('src/stylus/**/*.styl', ['stylus'])
    gulp.watch('src/img/*', ['imagemin'])
})

gulp.task('build', ['ioniconsDependences', 'useref', 'stylus', 'imagemin'])

gulp.task('default', ['build'])