app.controller('MainCtrl', ['$scope', function($scope) {
	$scope.windowH = {
		"height": window.innerHeight+'px'
	}
	
	$scope.products = [
		{
			name: 'Modern Black Armchair',
			value: '350',
			image: 'img/product-1.jpg'
		},
		{
			name: 'Modern Gray Armchair',
			value: '350',
			image: 'img/product-2.jpg'
		},
		{
			name: 'Light Blue Armchair',
			value: '350',
			image: 'img/product-3.jpg'
		},
		{
			name: 'Black and Wood Armchair',
			value: '350',
			image: 'img/product-4.jpg'
		}
	];
}])