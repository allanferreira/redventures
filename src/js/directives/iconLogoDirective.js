app.directive("iconLogo", function(){
	return {
		templateUrl: "views/components/iconLogo.html",
		replace: true,
		restrict: "E",
		transclude: false, 
		scope: {
			color: "@"
		}
	}
});