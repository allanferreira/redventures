app.directive("productList", function(){
	return {
		templateUrl: "views/components/productList.html",
		replace: true,
		restrict: "E",
		transclude: false, 
		scope: {
			name: "@",
			value: "@",
			image: "@"
		}
	}
});