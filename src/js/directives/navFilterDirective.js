app.directive("navFilter", function(){
	return {
		templateUrl: "views/components/navFilter.html",
		replace: true,
		restrict: "E",
		transclude: false
	}
});