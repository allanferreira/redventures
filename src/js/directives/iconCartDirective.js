app.directive("iconCart", function(){
	return {
		templateUrl: "views/components/iconCart.html",
		replace: true,
		restrict: "E",
		transclude: false, 
		scope: {
			color: "@"
		}
	}
});